use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn criterion_benchmark(criterion: &mut Criterion) {
    let post = serde_benchmark::example::example();
    let serialized = serde_benchmark::bson::to_vec(&post).unwrap();

    {
        let mut deserialize = criterion.benchmark_group("deserialize");

        deserialize.bench_function("non-serde", |bench| {
            bench.iter(|| {
                serde_benchmark::bson::from_slice::<serde_benchmark::example::Post>(black_box(
                    &serialized,
                ))
                .unwrap()
            });
        });

        deserialize.bench_function("serde", |bench| {
            bench.iter(|| {
                bson::from_slice::<serde_benchmark::example::Post>(black_box(&serialized)).unwrap()
            });
        });
    }

    {
        let mut serialize = criterion.benchmark_group("serialize");

        serialize.bench_function("non-serde", |bench| {
            bench.iter(|| serde_benchmark::bson::to_vec(black_box(&post)).unwrap());
        });

        serialize.bench_function("non-serde with_capacity", |bench| {
            bench.iter(|| {
                let mut output = Vec::with_capacity(1024);
                serde_benchmark::bson::Serialize::serialize(black_box(&post), &mut output).unwrap();
                output
            });
        });

        serialize.bench_function("serde", |bench| {
            bench.iter(|| bson::to_vec(black_box(&post)).unwrap());
        });
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
