use crate::bson::{
    deserialize_document, deserialize_type, serialize_document, serialize_field, Deserialize,
    Serialize,
};

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Post<'a> {
    pub body: &'a str,
    pub categories: Vec<i64>,
    pub published_at: i64,
    pub title: &'a str,
}

impl<'de> Deserialize<'de> for Post<'de> {
    fn deserialize(source: &mut &'de [u8]) -> Option<Self> {
        deserialize_document(source, |source| {
            let mut body = None;
            let mut categories = None;
            let mut published_at = None;
            let mut title = None;
            while let [type_id, rest @ ..] = source {
                let type_id = *type_id;
                match rest {
                    [b'b', b'o', b'd', b'y', 0, rest @ ..] => {
                        *source = rest;
                        body = Some(deserialize_type(type_id, source)?);
                    }
                    [b'c', b'a', b't', b'e', b'g', b'o', b'r', b'i', b'e', b's', 0, rest @ ..] => {
                        *source = rest;
                        categories = Some(deserialize_type(type_id, source)?);
                    }
                    [b'p', b'u', b'b', b'l', b'i', b's', b'h', b'e', b'd', b'_', b'a', b't', 0, rest @ ..] =>
                    {
                        *source = rest;
                        published_at = Some(deserialize_type(type_id, source)?);
                    }
                    [b't', b'i', b't', b'l', b'e', 0, rest @ ..] => {
                        *source = rest;
                        title = Some(deserialize_type(type_id, source)?);
                    }
                    _ => {
                        return None;
                    }
                }
            }
            Some(Post {
                body: body?,
                categories: categories?,
                published_at: published_at?,
                title: title?,
            })
        })
    }
}

impl Serialize for Post<'_> {
    fn serialize(&self, output: &mut Vec<u8>) -> Option<()> {
        serialize_document(output, |output| {
            serialize_field(&self.body, output, "body")?;
            serialize_field(&self.categories, output, "categories")?;
            serialize_field(&self.published_at, output, "published_at")?;
            serialize_field(&self.title, output, "title")?;
            Some(())
        })
    }

    fn type_id() -> u8 {
        3
    }
}

pub fn example() -> Post<'static> {
    Post {
        body: "This is an example.",
        categories: vec![1, 2, 200],
        published_at: 12345,
        title: "Hello, World",
    }
}

#[test]
fn test() {
    let post = example();
    let non_serde = crate::bson::to_vec(&post).unwrap();
    let serde = bson::to_vec(&post).unwrap();
    assert_eq!(serde, non_serde);
    let deserialized: Post = crate::bson::from_slice(&non_serde).unwrap();
    let reserialized = crate::bson::to_vec(&deserialized).unwrap();
    assert_eq!(non_serde, reserialized);
}
