use std::convert::{TryFrom, TryInto};

pub trait Deserialize<'de>
where
    Self: Sized,
{
    fn deserialize(source: &mut &'de [u8]) -> Option<Self>;
}

pub trait Serialize {
    fn serialize(&self, output: &mut Vec<u8>) -> Option<()>;

    fn type_id() -> u8;
}

impl<T: Serialize> Serialize for &T {
    fn serialize(&self, output: &mut Vec<u8>) -> Option<()> {
        (*self).serialize(output)
    }

    fn type_id() -> u8 {
        T::type_id()
    }
}

macro_rules! integer {
    ( $type:tt $type_id:tt ) => {
        impl<'de> Deserialize<'de> for $type {
            fn deserialize(source: &mut &'de [u8]) -> Option<Self> {
                let size = std::mem::size_of::<Self>();
                if source.len() < size {
                    None
                } else {
                    let value = $type::from_le_bytes(source[..size].try_into().unwrap());
                    *source = &source[size..];
                    Some(value)
                }
            }
        }

        impl Serialize for $type {
            fn serialize(&self, output: &mut Vec<u8>) -> Option<()> {
                output.extend_from_slice(&self.to_le_bytes());
                Some(())
            }

            fn type_id() -> u8 {
                $type_id
            }
        }
    };
}

integer!(i32 0x10);
integer!(i64 0x12);

impl<'de> Deserialize<'de> for &'de str {
    fn deserialize(source: &mut &'de [u8]) -> Option<Self> {
        let length = usize::try_from(i32::deserialize(source)?).ok()?;
        if length < 1 || source.len() < length {
            None
        } else {
            let value = std::str::from_utf8(&source[..length - 1]).ok()?;
            if source[length - 1] == 0 {
                *source = &source[length..];
                Some(value)
            } else {
                None
            }
        }
    }
}

impl Serialize for &str {
    fn serialize(&self, output: &mut Vec<u8>) -> Option<()> {
        i32::try_from(self.len() + 1).ok()?.serialize(output)?;
        output.extend_from_slice(self.as_bytes());
        output.push(0);
        Some(())
    }

    fn type_id() -> u8 {
        2
    }
}

impl<'de, T: Deserialize<'de> + Serialize> Deserialize<'de> for Vec<T> {
    fn deserialize(source: &mut &'de [u8]) -> Option<Self> {
        deserialize_document(source, |source| {
            let mut value = vec![];
            while let [type_id, rest @ ..] = source {
                if *type_id != T::type_id() {
                    return None;
                }
                *source = rest;
                loop {
                    let (character, rest) = source.split_first()?;
                    *source = rest;
                    if *character == 0 {
                        break;
                    }
                }
                value.push(T::deserialize(source)?);
            }
            Some(value)
        })
    }
}

impl<T: Serialize> Serialize for Vec<T> {
    fn serialize(&self, output: &mut Vec<u8>) -> Option<()> {
        serialize_document(output, |output| {
            for (item_index, item) in self.iter().enumerate() {
                output.push(T::type_id());
                serialize_array_key(item_index, output);
                output.push(0);
                item.serialize(output)?;
            }
            Some(())
        })
    }

    fn type_id() -> u8 {
        4
    }
}

fn serialize_array_key(mut item_index: usize, output: &mut Vec<u8>) {
    loop {
        output.push((item_index % 10) as u8 + b'0');
        if item_index < 10 {
            break;
        }
        item_index /= 10;
    }
}

pub fn deserialize_document<'de, T>(
    source: &mut &'de [u8],
    callback: impl FnOnce(&mut &'de [u8]) -> Option<T>,
) -> Option<T> {
    let length_size = std::mem::size_of::<i32>();
    if source.len() < length_size {
        return None;
    }
    let length = usize::try_from(i32::from_le_bytes(
        source[..length_size].try_into().unwrap(),
    ))
    .ok()?;
    if length < 5 || source.len() < length {
        return None;
    }
    let value = callback(&mut &source[length_size..length - 1])?;
    if source[length - 1] == 0 {
        *source = &source[length..];
        Some(value)
    } else {
        None
    }
}

pub fn deserialize_type<'de, T: Deserialize<'de> + Serialize>(
    type_id: u8,
    source: &mut &'de [u8],
) -> Option<T> {
    if type_id == T::type_id() {
        T::deserialize(source)
    } else {
        None
    }
}

pub fn from_slice<'de, T: Deserialize<'de>>(mut source: &'de [u8]) -> Option<T> {
    let value = T::deserialize(&mut source)?;
    if source.is_empty() {
        Some(value)
    } else {
        None
    }
}

pub fn serialize_document(
    output: &mut Vec<u8>,
    callback: impl FnOnce(&mut Vec<u8>) -> Option<()>,
) -> Option<()> {
    let length_offset = output.len();
    output.extend_from_slice(&[0, 0, 0, 0]);
    callback(output)?;
    output.push(0);
    let length = output.len() - length_offset;
    output[length_offset..length_offset + 4]
        .copy_from_slice(&i32::try_from(length).ok()?.to_le_bytes());
    Some(())
}

pub fn serialize_field<T: Serialize>(value: &T, output: &mut Vec<u8>, key: &str) -> Option<()> {
    output.push(T::type_id());
    output.extend_from_slice(key.as_bytes());
    output.push(0);
    value.serialize(output)
}

pub fn to_vec<T: Serialize>(value: &T) -> Option<Vec<u8>> {
    let mut output = vec![];
    value.serialize(&mut output)?;
    Some(output)
}
